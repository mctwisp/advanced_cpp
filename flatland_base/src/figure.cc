#include <iostream>
#include "figure.hh"

using namespace std;

Figure::Figure()
{
    class_name="figure";
}

double Figure::get_area() const
{
    return area;
}

void Figure::description()
{
    cout<<"A generic "<<class_name<<" of area "<<get_area()<<endl;
}

