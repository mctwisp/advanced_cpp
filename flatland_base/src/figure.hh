
class Figure
{
    private:
        std::string class_name;
        double area;

    public:
        Figure();
        double get_area() const;
        void description();
};
